// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GenericPlatform/GenericPlatform.h"
#include "Containers/Array.h"
#include "Math/UnrealMathUtility.h"
#include "BFL_TilePuzzleHelper.generated.h"


/**
 *
 */
UCLASS()
class HALLS_OF_BELIAL_API UBFL_TilePuzzleHelper : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static TArray<int32> PathGenerator(int32 rows, int32 cols);
};
