// Fill out your copyright notice in the Description page of Project Settings.


#include "./BFL_TilePuzzleHelper.h"

TArray<int32> UBFL_TilePuzzleHelper::PathGenerator(int32 rows, int32 cols) {
    int32 size = rows * cols;

    TArray<int32> path;
    path.Init(0, size);

    int32 start_col = FMath::RandRange(0, cols - 1);
    int32 curr_col = start_col;
    int32 prev_col = curr_col;

    // set first two cells of path
    for (int32 col = 0; col < cols; col++) {
        if (col == start_col) {
            path[col] = 1;
        }
        else {
            path[col] = -1;
        }
    }
    path[0 * rows + start_col] = 1;
    path[1 * rows + start_col] = 1;

    int32 curr_row = 1;
    int32 prev_row = 0;

    int32 curr_up = 0;
    int32 curr_down = 0;
    int32 curr_right = 0;
    int32 curr_left = 0;

    int32 prev_up = 0;
    int32 prev_down = 0;
    int32 prev_left = 0;
    int32 prev_right = 0;

    bool can_up = true;
    bool can_down = true;
    bool can_left = true;
    bool can_right = true;

    int32 prev_dir = 0;
    int32 dir = 0;
    int32 consec_right_turn = 0;
    int32 consec_left_turn = 0;

    int32 count = 0;

    while (curr_row < (rows - 1) && count < 50) {
        count++;
        // set traverseability from current node
        curr_up = curr_row + 1;
        curr_down = curr_row - 1;
        curr_right = curr_col + 1;
        curr_left = curr_col - 1;

        can_up = (curr_up >= 0 && curr_up < rows)
            ? (path[curr_up * rows + curr_col] == 0)
            : false;
        can_up = can_up && (prev_row != curr_row + 1);
        can_down = (curr_down >= 0 && curr_down < rows)
            ? (path[curr_down * rows + curr_col] == 0)
            : false;
        can_down = can_down && (curr_down != 0) && (curr_col > 1) &&
            (curr_col < cols - 2) && (prev_row != curr_row - 1);
        can_left = (curr_left >= 0 && curr_left < cols)
            ? (path[curr_row * rows + curr_left] == 0)
            : false;
        can_left = can_left && (prev_col != curr_col - 1);
        can_left = (curr_row <= 2) ? (can_left && curr_col <= start_col) : can_left;
        can_right = (curr_right < cols && curr_right < cols)
            ? (path[curr_row * rows + curr_right] == 0)
            : false;
        can_right = can_right && (prev_col != curr_col + 1);
        can_right =
            (curr_row <= 2) ? (can_right && curr_col >= start_col) : can_right;

        // select direction and update turning tracker
        prev_dir = dir;
        dir = FMath::RandRange(0, 3);

        switch (dir) {
        case 0:
            if (prev_dir == 1) {
                consec_left_turn++;
                consec_right_turn = 0;
                if (!can_right && consec_left_turn == 2) {
                    dir = 2;
                }
            }
            else if (prev_dir == 3) {
                consec_right_turn++;
                consec_left_turn = 0;
                if (!can_left && consec_right_turn == 2) {
                    dir = 2;
                }
            }
            break;
        case 1:
            if (prev_dir == 2) {
                consec_left_turn++;
                consec_right_turn = 0;
                if (!can_down && consec_left_turn == 2) {
                    dir = 3;
                }
            }
            else if (prev_dir == 0) {
                consec_right_turn++;
                consec_left_turn = 0;
                if (!can_up && consec_right_turn == 2) {
                    dir = 3;
                }
            }
        case 2:
            if (prev_dir == 3) {
                consec_left_turn++;
                consec_right_turn = 0;
                if (!can_right && consec_right_turn == 2) {
                    dir = 0;
                }
            }
            else if (prev_dir == 1) {
                consec_right_turn++;
                consec_left_turn = 0;
                if (!can_left && consec_left_turn == 2) {
                    dir = 0;
                }
            }
        case 3:
            if (prev_dir == 0) {
                consec_left_turn++;
                consec_right_turn = 0;
                if (!can_up && consec_left_turn == 2) {
                    dir = 1;
                }
            }
            else if (prev_dir == 2) {
                consec_right_turn++;
                consec_left_turn = 0;
                if (!can_up && consec_right_turn == 2) {
                    dir = 1;
                }
            }
        }

        bool selected = false;

        int32 count2 = 0;
        while (!selected && count2 < 2) {
            count2++;
            // decide next cell to traverse

            // when if fails, will continue until finds a traverseable direction
            switch (dir) {
            case 0:
                if (can_up) {
                    selected = true;
                    prev_row = curr_row;
                    prev_col = curr_col;

                    path[curr_up * rows + curr_col] = 1;
                    curr_row = curr_up;
                    break;
                }
            case 1:
                if (can_right) {
                    selected = true;
                    prev_row = curr_row;
                    prev_col = curr_col;

                    path[curr_row * rows + curr_right] = 1;
                    curr_col = curr_right;
                    break;
                }
            case 2:
                if (can_down) {
                    selected = true;
                    prev_row = curr_row;
                    prev_col = curr_col;

                    path[curr_down * rows + curr_col] = 1;
                    curr_row = curr_down;
                    break;
                }
            case 3:
                if (can_left) {
                    selected = true;
                    prev_row = curr_row;
                    prev_col = curr_col;

                    path[curr_row * rows + curr_left] = 1;
                    curr_col = curr_left;
                    break;
                }
            default:
                dir = 0;
            }
        }
        // set cells surrounding previous path cell as non-traverseable
        prev_up = (prev_row + 1) * rows + prev_col;
        prev_down = (prev_row - 1) * rows + prev_col;
        prev_left = prev_row * rows + (prev_col - 1);
        prev_right = prev_row * rows + (prev_col + 1);

        if (prev_col > 0 && prev_col < cols - 1) {
            path[prev_right] = (path[prev_right] == 0) ? -1 : path[prev_right];
            path[prev_left] = (path[prev_left] == 0) ? -1 : path[prev_left];
        }
        else if (prev_col > 0) {
            path[prev_left] = (path[prev_left] == 0) ? -1 : path[prev_left];
        }
        else if (prev_col < cols - 1) {
            path[prev_right] = (path[prev_right] == 0) ? -1 : path[prev_right];
        }

        if (prev_row > 0 && prev_row < rows - 1) {
            path[prev_up] = (path[prev_up] == 0) ? -1 : path[prev_up];
            path[prev_down] = (path[prev_down] == 0) ? -1 : path[prev_down];
        }
        else if (prev_row > 0) {
            path[prev_down] = (path[prev_down] == 0) ? -1 : path[prev_down];
        }
        else if (prev_row < rows - 1) {
            path[prev_up] = (path[prev_up] == 0) ? -1 : path[prev_up];
        }
    }
    return path;
}

